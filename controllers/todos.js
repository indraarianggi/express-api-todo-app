const { validationResult } = require("express-validator");
const { Todo } = require("../models");

const create = async (req, res) => {
    // Error checking
    const error = validationResult(req);
    if (!error.isEmpty()) {
        return res.status(400).json({
            success: false,
            errors: error.array(),
        });
    }

    const { title, description } = req.body;

    try {
        const todo = await Todo.create({
            title,
            description,
        });

        res.json({
            success: true,
            message: "Berhasil menambahkan tugas baru",
            result: todo,
        });
    } catch (error) {
        console.log("Error", error);
        return res.status(500).json({
            success: false,
            message: "Internal Server Error!",
            errors: [
                {
                    msg: error.message || error,
                },
            ],
        });
    }
};

const findAll = async (req, res) => {
    try {
        const todos = await Todo.findAll({ order: [["createdAt", "ASC"]] });

        res.json({
            success: true,
            message: "Berhasil mendapatkan semua tugas",
            results: todos,
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({
            success: false,
            message: "Internal Server Error!",
            errors: [
                {
                    msg: error.message || error,
                },
            ],
        });
    }
};

const complete = async (req, res) => {
    const todoId = req.params.todo_id;

    try {
        let todo = await Todo.findOne({ where: { id: todoId } });

        // if todo not found
        if (!todo) {
            return res.status(404).json({
                success: false,
                message: "Not Found",
                errors: [{ msg: "Tugas tidak ditemukan" }],
            });
        }

        await todo.update({ complete: true });

        const todos = await Todo.findAll({ order: [["createdAt", "ASC"]] });

        res.json({
            success: true,
            message: "Tugas telah diselesaikan",
            results: todos,
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({
            success: false,
            message: "Internal Server Error!",
            errors: [
                {
                    msg: error.message || error,
                },
            ],
        });
    }
};

const destroy = async (req, res) => {
    const todoId = req.params.todo_id;

    try {
        await Todo.destroy({ where: { id: todoId } });

        const todos = await Todo.findAll({ order: [["createdAt", "ASC"]] });

        res.json({
            success: true,
            message: "Tugas berhasl di hapus",
            results: todos,
        });
    } catch (error) {
        console.error(error);
        res.status(500).json({
            success: false,
            message: "Internal Server Error!",
            errors: [
                {
                    msg: error.message || error,
                },
            ],
        });
    }
};

/**
 * Export
 */
module.exports = {
    create,
    findAll,
    complete,
    destroy,
};
