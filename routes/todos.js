var express = require("express");
var router = express.Router();
const { check } = require("express-validator");

const todoControllers = require("../controllers/todos");

/* Create/add new todo */
router.post(
    "/add",
    [
        check("title").not().isEmpty().withMessage("Judul tugas harus diisi"),
        check("description")
            .not()
            .isEmpty()
            .withMessage("Deskripsi tugas harus diisi"),
    ],
    todoControllers.create
);

router.get("/", todoControllers.findAll);

router.put("/complete/:todo_id", todoControllers.complete);

router.delete("/:todo_id", todoControllers.destroy);

module.exports = router;
